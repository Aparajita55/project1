<?php
require_once('./../db/config.php');

class DataBaseHandler {
 	public $con;

 	public function __construct() 
 	{
 		$this->con = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME); 
 		
 		if (!$this->con) {
 			echo "Error in Connecting ".mysqli_connect_error();
        }
    }

   	public function insert($fname, $email, $uname, $password, $token, $verify_flag) 
   	{
        return mysqli_query($this->con,"insert into register(fname,email,uname,password,token,verify_flag) values('$fname','$email','$uname','$password','$token','$verify_flag')");
    }

  public function login($username, $password)
  {
 
        $sql = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";
        $query = $this->con->query($sql);
 
        if($query->num_rows > 0) {
            $row = $query->fetch_array();
            return $row['id'];
        
        } else {
            
            return false;
        }
    }
 
    public function details($sql)
    {
 
        $query = $this->con->query($sql);
 
        $row = $query->fetch_array();
 
        return $row;       
    }
 
    public function escape_string($value)
    {
 
        return $this->con->real_escape_string($value);
    }
}

