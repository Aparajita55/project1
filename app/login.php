

   <?php
//start session
session_start();
 
include_once('function.php');
 
$user = new DataBaseHandler();
 
if(isset($_POST['login'])) {
  $username = $user->escape_string($_POST['username']);
  $password = $user->escape_string($_POST['password']);
 
  $auth = $user->login($username, $password);
 
  if(!$auth){
    $_SESSION['message'] = 'Invalid username or password';
      header('location: /Project1/public_html/index.php');
  }
  else{
    $_SESSION['user'] = $auth;
    header('location: /Project1/public_html/home.php');
  }
}
?>